
$(function(){

    var model = {
        // renomear
        dat: [

        ],
        read_json: function() {

            $.ajax({
                url: "cards/info.json",
                dataType: 'json',
                async: false,
                success: function(data) {
                    $(data["cards"]).each(function(){
                        var card_info = $(this)[0];
                        model.set_data(card_info);
                    });
                }
            });

        },
        set_data: function(d) {
            model.dat.push(d);
        },
        get_data: function() {
            return model.dat;
        },
        filter_data: function(conteudo) {
            var $d = model.dat;
            var $filter = [];
            var $c = conteudo.toLowerCase();
            $($d).each(function(i){
                if(
                    $d[i].name.toLowerCase().indexOf($c) >= 0 ||
                    $d[i].description.toLowerCase().indexOf($c) >= 0 ||
                    $d[i].button_text.toLowerCase().indexOf($c) >= 0
                ) {
                    $filter.push($d[i]);
                }
            });

            return $filter;
        },
        init: function() {
            model.read_json();
        }
    };


    var octopus = {
        init: function() {
            model.init();
            view.init();
            octopus.create_card(model.get_data());
        },
        create_card: function(d) {
            $.each(d, function(i){
                view.create_card(d[i].name, d[i].description, d[i].button_text);
            });
        },
        filter_data: function(c) {
            var d = model.filter_data(c);
            $.each(d, function(i){
                view.create_card(d[i].name, d[i].description, d[i].button_text);
            });
        },
        recreate_cards: function() {
            octopus.create_card(model.get_data());
        }
    };

    // Renomear
    var view = {
        init: function() {
            this.container = $(".flex-wrapper");
            this.search_field = $("#search_field");
            view.events();
        },
        events: function(){
            this.search_field.on('input',function(){
                var $conteudo = $(this).val();
                view.clean_cards();


                if($conteudo.length == 0){
                    octopus.recreate_cards();
                }else {
                    octopus.filter_data($conteudo);
                }

            });
        },
        create_card: function(name, desc, bt_txt){
            var $card = $("<div>").addClass("card");
            var $h2 = $("<h2>").addClass("title").text(name);
            var $desc = $("<div>").addClass("description").text(desc);
            var $button = $("<button>").text(bt_txt);


            $card.append($h2).append($desc).append($button);
            this.container.append($card);
        },
        clean_cards: function() {
            this.container.empty();
        }
    };


    octopus.init();
});
